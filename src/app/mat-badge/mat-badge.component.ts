import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mat-badge',
  templateUrl: './mat-badge.component.html',
  styleUrls: ['./mat-badge.component.css'],
})
export class MatBadgeComponent implements OnInit {
  notifications = 10;
  notification = 0;
  constructor() {}

  ngOnInit(): void {}
}
