import { Component, OnInit } from '@angular/core';

import { PopupWindowComponent } from '../popup-window/popup-window.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-popups',
  templateUrl: './popups.component.html',
  styleUrls: ['./popups.component.css'],
})
export class PopupsComponent implements OnInit {
  animal: string;
  name: string;

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openDialog(): void {
    debugger;
    const dialogRef = this.dialog.open(PopupWindowComponent, {
      width: '250px',
      data: { name: this.name, animal: this.animal },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
}
