import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular_material_basics';
  notifications = 10;
  notification = 0;
  showSpin = false;
  opened = false;

  selected = 'option2';
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  disabled = false;
}
