import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.css'],
})
export class ProgressSpinnerComponent implements OnInit {
  showSpin = false;
  opened = false;
  constructor() {}

  ngOnInit(): void {}

  showSpineer = () => {
    this.showSpin = true;
    setTimeout(() => {
      this.showSpin = false;
    }, 5000);
  };
}
