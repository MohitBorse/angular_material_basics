import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { MatButtonComponent } from './mat-button/mat-button.component';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { MatIconComponent } from './mat-icon/mat-icon.component';
import { MatBadgeComponent } from './mat-badge/mat-badge.component';
import { ProgressSpinnerComponent } from './progress-spinner/progress-spinner.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { MenuComponent } from './menu/menu.component';
import { ListComponent } from './list/list.component';
import { DividerComponent } from './divider/divider.component';
import { ExpansionComponent } from './expansion/expansion.component';
import { CardComponent } from './card/card.component';
import { TabsComponent } from './tabs/tabs.component';
import { StepperComponent } from './stepper/stepper.component';
import { InputComponent } from './input/input.component';
import { SelectComponent } from './select/select.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { RadioComponent } from './radio/radio.component';
import { DateComponent } from './date/date.component';
import { PopupsComponent } from './popups/popups.component';
import { TableComponent } from './table/table.component';
import { PopupWindowComponent } from './popup-window/popup-window.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    MatButtonComponent,
    ToggleButtonComponent,
    MatIconComponent,
    MatBadgeComponent,
    ProgressSpinnerComponent,
    ToolBarComponent,
    SideNavComponent,
    MenuComponent,
    ListComponent,
    DividerComponent,
    ExpansionComponent,
    CardComponent,
    TabsComponent,
    StepperComponent,
    InputComponent,
    SelectComponent,
    AutocompleteComponent,
    CheckboxComponent,
    RadioComponent,
    DateComponent,
    PopupsComponent,
    TableComponent,
    PopupWindowComponent,
    LoginComponent,
    RegistrationComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
  ],
  entryComponents: [PopupsComponent, PopupWindowComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
