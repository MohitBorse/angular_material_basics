import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { MatButtonComponent } from './mat-button/mat-button.component';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { MatIconComponent } from './mat-icon/mat-icon.component';
import { MatBadgeComponent } from './mat-badge/mat-badge.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { CardComponent } from './card/card.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DateComponent } from './date/date.component';
import { DividerComponent } from './divider/divider.component';
import { ExpansionComponent } from './expansion/expansion.component';
import { InputComponent } from './input/input.component';
import { ListComponent } from './list/list.component';
import { MenuComponent } from './menu/menu.component';
import { PopupsComponent } from './popups/popups.component';
import { ProgressSpinnerComponent } from './progress-spinner/progress-spinner.component';
import { RadioComponent } from './radio/radio.component';
import { SelectComponent } from './select/select.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { StepperComponent } from './stepper/stepper.component';
import { TableComponent } from './table/table.component';
import { TabsComponent } from './tabs/tabs.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { MatDatepickerContent } from '@angular/material/datepicker';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';

const routes: Routes = [
  { path: 'button-component', component: MatButtonComponent },
  { path: 'toggle-btn-component', component: ToggleButtonComponent },
  { path: 'mat-icon', component: MatIconComponent },
  { path: 'mat-badge', component: MatBadgeComponent },
  { path: 'auto-complete', component: AutocompleteComponent },
  { path: 'mat-card', component: CardComponent },
  { path: 'mat-checkbox', component: CheckboxComponent },
  { path: 'mat-divider', component: DividerComponent },
  { path: 'mat-expansion', component: ExpansionComponent },
  { path: 'mat-input', component: InputComponent },
  { path: 'mat-list', component: ListComponent },
  { path: 'mat-menu', component: MenuComponent },
  { path: 'mat-popup', component: PopupsComponent },
  { path: 'mat-progress', component: ProgressSpinnerComponent },
  { path: 'mat-radio', component: RadioComponent },
  { path: 'mat-select', component: SelectComponent },
  { path: 'mat-sidenav', component: SideNavComponent },
  { path: 'mat-stepper', component: StepperComponent },
  { path: 'mat-table', component: TableComponent },
  { path: 'mat-tabs', component: TabsComponent },
  { path: 'mat-tool-bar', component: ToolBarComponent },
  { path: 'mat-date', component: DateComponent },
  { path: '*', component: AppComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'registration', component: RegistrationComponent, pathMatch: 'full' },
]; // sets up routes constant where you define your routes

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
